import PromiseRouter from "express-promise-router";
import {ICandidate} from "../modules/candidates/candidates-model";
import {CandidatesModule} from "../modules/candidates/candidates-module";
import {IQuiz} from "../modules/quizes/quizes-model";
import {QuizesModule} from "../modules/quizes/quizes-module";
import {SotzyometryModule} from "../modules/sotzyometry/sotzyometry-module";
import {ISotzyometry} from "../modules/sotzyometry/sotzyometry-model";
import {calcNineBoxResults, createChartData, IInvalidData, NineBoxDataDict} from "../utils";
import {ICycle} from "../modules/cycles/cycles-model";
import {CyclesModule} from "../modules/cycles/cycles-module";


export function createIndexRoute() {
    const indexRoute = PromiseRouter();

    indexRoute.get('/isAlive', function (req, res, next) {
        res.status(200).json("wowowowo");
    });
    indexRoute.post('/login', function (req, res, next) {
        const username = req.body['username'];
        const password = req.body['password'];

        if (username === 'adminLisha' && password === 'adminLisha123') {

            res.status(200).json("token");
        } else {
            res.status(500).json();
        }
    });

    // SOTZYOMETRY
    indexRoute.post('/uploadSotzyometryResults', async function (req, res, next) {
        const results: any[] = req.body;
        const cycle = await CyclesModule.getLastCycle();

        await results.forEach(async (sotzyometry: any) => {
            //TODO: change hebrew names
            const _sotzyometry: ISotzyometry = {
                cycleId: cycle._id,
                MA: sotzyometry['מ"א'],
                potentialScore: sotzyometry['פוטנציאל'],
                bitzuaScore: sotzyometry['ביצוע'],
            };
            await SotzyometryModule.saveSotzyometry(_sotzyometry);
        });

        res.status(200).json();
    });
    indexRoute.post('/addSotzyometry', async function (req, res, next) {
        const sotzyometry: ISotzyometry = req.body;

        try {
            await SotzyometryModule.saveSotzyometry(sotzyometry);

            res.status(200).json();
        } catch (e) {
            res.status(500).json(e);
        }
    });

    // CANDIDATES
    indexRoute.get('/getCandidates', async function (req, res, next) {
        const candidates: ICandidate[] = await CandidatesModule.getAllCandidates();

        res.status(200).json(candidates);
    });
    indexRoute.get('/getPeople', async function (req, res, next) {
        const candidates: ICandidate[] = await CandidatesModule.getAllPeople();

        res.status(200).json(candidates);
    });
    indexRoute.post('/uploadCandidates', async function (req, res, next) {
        const candidates: any[] = req.body;

        await candidates.forEach(async (candidate: any) => {
            const _candidate: ICandidate = {
                MA: candidate['מ"א'],
                name: candidate['שם'],
                tafkid: candidate['תפקיד'],
                age: candidate['גיל'],
                vetek: candidate['ותק'],
                gender: candidate['מין'],
                darga: candidate['שם דרגה'],
                slavesSum: candidate['כמות משרתים'],
                merkaz: candidate['מרכז'],
                anaf: candidate['ענף'],
                mador: candidate['מדור']
            };
            await CandidatesModule.saveCandidate(_candidate);
        });

        res.status(200).json();
    });
    indexRoute.post('/addCandidate', async function (req, res, next) {
        const candidate: ICandidate = req.body;

        try {
            await CandidatesModule.saveCandidate(candidate);

            res.status(200).json();
        } catch (e) {
            res.status(500).json(e);
        }
    });
    indexRoute.post('/deleteAllCandidates', async function (req, res, next) {
        try {
            await CandidatesModule.deleteAllCandidates();

            res.status(200).json();
        } catch (e) {
            res.status(500).json(e);
        }
    });

    // CYCLES
    indexRoute.get('/getCycles', async function (req, res, next) {
        const cycles: ICycle[] = await CyclesModule.getAllCycles();

        res.status(200).json(cycles);
    });
    indexRoute.post('/createCycle', async function (req, res, next) {
        const cycleName: string = req.body['name'];

        const wow = await CyclesModule.createCycle(cycleName);

        res.status(200).json();
    });
    indexRoute.post('/deleteCycle', async function (req, res, next) {
        const cycleID: string = req.body;

        await CyclesModule.deleteCycle(cycleID);

        res.status(200).json();
    });

    // QUIZES
    indexRoute.post('/saveQuiz', async function (req, res, next) {
        const quiz: IQuiz = req.body;

        try {
            await QuizesModule.saveQuiz(quiz);

            res.status(200).json();
        } catch (e: any) {
            res.status(505).json(e.message);
        }
    });

    indexRoute.post('/getNineBoxResults', async function (req, res, next) {
        const chosenCandidates: ICandidate[] = req.body['chosenCandidates'];
        const chosenCycles: ICycle[] = req.body['chosenCycles'];
        const nineBoxDataDict: NineBoxDataDict = {};
        const invalidData: IInvalidData[] = [];

        try {
            await Promise.all(chosenCycles.map(async (cycle: ICycle) => {
                await Promise.all(chosenCandidates.map(async (candidate: ICandidate) => {
                    const sotzyometryResult: ISotzyometry | null = await SotzyometryModule.getSotzyometry(candidate.MA, cycle._id);
                    const quizes: IQuiz[] = await QuizesModule.getQuizes(candidate.MA, cycle._id);

                    const hasDirectCommander = quizes.length > 0 && quizes.some((quiz: IQuiz) => quiz.isDirectCommander === true);
                    const hasSotzyometry = sotzyometryResult !== null;

                    if (hasDirectCommander && hasSotzyometry) {
                        const results = calcNineBoxResults(quizes, sotzyometryResult);
                        if (!nineBoxDataDict[cycle.name]) {
                            nineBoxDataDict[cycle.name] = [];
                        }
                        nineBoxDataDict[cycle.name].push({
                            candidate: candidate,
                            cycle: cycle,
                            quizes: quizes,
                            sotzyomeryResult: sotzyometryResult,
                            results: results
                        });
                    } else {
                        invalidData.push({
                            name: candidate.name + '-' + cycle.name,
                            hasDirectCommander: hasDirectCommander,
                            hasSotzyometry: hasSotzyometry
                        })
                    }
                }));
            }));

            const chartData = createChartData(nineBoxDataDict);

            res.status(200).json({chartData: chartData, nineBoxDataDict: nineBoxDataDict, invalidData: invalidData});
        } catch (e: any) {
            res.status(505).json(e.message);
        }
    });

    return indexRoute;
}
