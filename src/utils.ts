import {ICandidate} from "./modules/candidates/candidates-model";
import {ISotzyometry} from "./modules/sotzyometry/sotzyometry-model";
import {IQuiz} from "./modules/quizes/quizes-model";
import {ICycle} from "./modules/cycles/cycles-model";

export interface INineBoxData {
    candidate: ICandidate,
    cycle: ICycle,
    sotzyomeryResult: ISotzyometry | null,
    quizes: IQuiz[],
    results: INineBoxResults | null
}

export interface INineBoxResults {
    bitzua: number,
    potential: number
}

export interface IInvalidData {
    name: string,
    hasDirectCommander: boolean,
    hasSotzyometry: boolean
}

export type NineBoxDataDict = { [id: string]: INineBoxData[] };

export const calcNineBoxResults = (quizes: IQuiz[], sotzyometry: ISotzyometry): INineBoxResults => {
    let directCommandersSum = 0;
    let bitzuaQuizesScore = 0;
    let potentialQuizesScore = 0;
    let bitzuaQuizesScoreDirectCommander = 0;
    let potentialQuizesScoreDirectCommander = 0;

    quizes.forEach((quiz: IQuiz) => {
        if (quiz.isDirectCommander) {
            directCommandersSum++;
            bitzuaQuizesScoreDirectCommander += (quiz.bitzua1 + quiz.bitzua2 + quiz.bitzua3) / 3;
            potentialQuizesScoreDirectCommander += (quiz.potential1 + quiz.potential2 + quiz.potential3) / 3;
        } else {
            bitzuaQuizesScore += (quiz.bitzua1 + quiz.bitzua2 + quiz.bitzua3) / 3;
            potentialQuizesScore += (quiz.potential1 + quiz.potential2 + quiz.potential3) / 3;
        }
    });

    const finalBitzuaScore = ((bitzuaQuizesScoreDirectCommander / directCommandersSum) * 0.4) + ((bitzuaQuizesScore / (quizes.length - directCommandersSum)) * 0.3) + (sotzyometry.bitzuaScore * 0.3);
    const finalPotentialScore = ((potentialQuizesScoreDirectCommander / directCommandersSum) * 0.4) + ((potentialQuizesScore / (quizes.length - directCommandersSum)) * 0.3) + (sotzyometry.potentialScore * 0.3);


    return {potential: finalPotentialScore, bitzua: finalBitzuaScore};
};

export const createChartData = (nineBoxDataDict: NineBoxDataDict): any => {
    let chartData: any[] = [];

    Object.keys(nineBoxDataDict).forEach((key: any) => {
        const nineBoxData: INineBoxData[] = nineBoxDataDict[key];
        const cycleColor = '#'+(0x1000000+Math.random()*0xffffff).toString(16).substr(1,6);

        nineBoxData.forEach((data: INineBoxData) => {
            if (data.results) {
                chartData.push({
                    color: cycleColor,
                    name: data.candidate.name + '-' + data.cycle.name,
                    x: data.results.bitzua,
                    y: data.results.potential
                });
            }
        });
    });

    const chartOptions = {
        theme: "dark2",
        animationEnabled: true,
        title: {
            text: "גרף 9 בקופסא"
        },
        axisX: {
            title: "ביצוע",
            titleFontSize: 13,
            minimum: 1,
            maximum: 7,
            stripLines: [
                {
                    value: 1,
                    color: "black"
                },
                {
                    value: 3,
                    color: "black"
                },
                {
                    value: 5,
                    color: "black"
                },
                {
                    value: 6.999,
                    color: "black"
                }
            ]
        },
        axisY: {
            title: "פוטנציאל",
            titleFontSize: 13,
            minimum: 1,
            maximum: 7,
            interval: 1,
            gridThickness: 0,
            stripLines: [
                {
                    value: 1,
                    color: "black"
                },
                {
                    value: 3,
                    color: "black"
                },
                {
                    value: 5,
                    color: "black"
                },
                {
                    value: 7,
                    color: "black"
                }
            ]
        },
        data: [{
            type: "scatter",
            markerSize: 50,
            indexLabel: "{name}",
            color: "{color}",
            toolTipContent: "<b>{name}</b> <br/> <span>ביצוע: {x}</span> <br/> <span>פוטנציאל: {y}</span>",
            dataPoints: chartData
        }]
    };

    return chartOptions;
};