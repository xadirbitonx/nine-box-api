import {QuizesModel, IQuiz} from "./quizes-model";
import {CyclesModule} from "../cycles/cycles-module";

export class QuizesModule {
    private static quizesModel: QuizesModel;

    public static init() {
        QuizesModule.quizesModel = new QuizesModel();
    }

    public static async saveQuiz(quiz: IQuiz): Promise<void> {
        const cycle = await CyclesModule.getLastCycle();
        quiz.cycleId = cycle._id;
        await QuizesModule.quizesModel.saveQuiz(quiz);
    }

    public static async getQuizes(MA: number, cycleId: string): Promise<IQuiz[]> {
        return await QuizesModule.quizesModel.getQuizes(MA, cycleId);
    }
}