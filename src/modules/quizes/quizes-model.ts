import * as mongoose from "mongoose";
import {Model, Schema} from "mongoose";
import {ICycle} from "../cycles/cycles-model";

export class QuizesModel {
    private static model: Model<IQuiz>;

    public constructor() {
        QuizesModel.model = mongoose.model<IQuiz>('quizes', quizSchema);
    }

    public async saveQuiz(quiz: IQuiz): Promise<void> {
        const existingQuiz = await QuizesModel.model.findOne({cycleId: quiz.cycleId, commanderMA: quiz.commanderMA, MA: quiz.MA});
        if (existingQuiz) {
            throw new Error('already filed a quiz on said candidate');
        } else {
            await QuizesModel.model.create(quiz);
        }
    }

    public async getQuizes(MA: number, cycleId: string): Promise<IQuiz[]> {
        return QuizesModel.model.find({MA: MA, cycleId: cycleId}).exec();
    }
}

export interface IQuiz {
    cycleId: string,
    commanderMA: number,
    isDirectCommander: boolean
    MA: number,
    bitzua1: number,
    bitzua2: number,
    bitzua3: number,
    potential1: number,
    potential2: number,
    potential3: number
}

const quizSchema = new Schema<IQuiz>({
    cycleId: String,
    commanderMA: Number,
    isDirectCommander: Boolean,
    MA: Number,
    bitzua1: Number,
    bitzua2: Number,
    bitzua3: Number,
    potential1: Number,
    potential2: Number,
    potential3: Number
});
