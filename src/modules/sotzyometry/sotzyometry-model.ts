import * as mongoose from "mongoose";
import {Model, Schema} from "mongoose";

export class SotzyometryModel {
    private static model: Model<ISotzyometry>;

    public constructor() {
        SotzyometryModel.model = mongoose.model<ISotzyometry>('sotzyometries', sotzyometrySchema);
    }

    public async getSotzyometry(MA: number, cycleId: string): Promise<ISotzyometry | null> {
        return SotzyometryModel.model.findOne({MA: MA, cycleId: cycleId}).exec();
    }

    public async saveSotzyometry(sotzyometry: ISotzyometry): Promise<void> {
        await SotzyometryModel.model.create(sotzyometry);
    }
}

export interface ISotzyometry {
    cycleId: string,
    MA: number,
    potentialScore: number,
    bitzuaScore: number
}

const sotzyometrySchema = new Schema<ISotzyometry>({
    cycleId: String,
    MA: Number,
    potentialScore: Number,
    bitzuaScore: Number
});
