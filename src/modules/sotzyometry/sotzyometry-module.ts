import {SotzyometryModel, ISotzyometry} from "./sotzyometry-model";
import {CyclesModule} from "../cycles/cycles-module";

export class SotzyometryModule {
    private static sotzyometryModel: SotzyometryModel;

    public static init() {
        SotzyometryModule.sotzyometryModel = new SotzyometryModel();
    }

    public static async getSotzyometry(MA: number, cycleId: string): Promise<ISotzyometry | null> {
        return await SotzyometryModule.sotzyometryModel.getSotzyometry(MA, cycleId);
    }

    public static async saveSotzyometry(sotzyometry: ISotzyometry): Promise<void> {
        await SotzyometryModule.sotzyometryModel.saveSotzyometry(sotzyometry);
    }
}