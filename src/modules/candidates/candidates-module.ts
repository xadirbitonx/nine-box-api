import {CandidatesModel, ICandidate} from "./candidates-model";

export class CandidatesModule {
    private static candidatesModel: CandidatesModel;

    public static init() {
        CandidatesModule.candidatesModel = new CandidatesModel();
    }

    public static async getAllCandidates(): Promise<ICandidate[]> {
        return await CandidatesModule.candidatesModel.getAllCandidates();
    }

    public static async getAllPeople(): Promise<ICandidate[]> {
        return await CandidatesModule.candidatesModel.getAllPeople();
    }

    public static async saveCandidate(candidate: ICandidate): Promise<void> {
        await CandidatesModule.candidatesModel.saveCandidate(candidate);
    }

    public static async deleteAllCandidates(): Promise<void> {
        await CandidatesModule.candidatesModel.deleteAllCandidates();
    }
}