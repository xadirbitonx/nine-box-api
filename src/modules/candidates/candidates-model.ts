import * as mongoose from "mongoose";
import {Model, Schema} from "mongoose";

export class CandidatesModel {
    private static model: Model<ICandidate>;

    public constructor() {
        CandidatesModel.model = mongoose.model<ICandidate>('candidates', candidateSchema);
    }

    public async getAllCandidates(): Promise<ICandidate[]> {
        return CandidatesModel.model.find({isDeleted: false}).exec();
    }

    public async getAllPeople(): Promise<ICandidate[]> {
        return CandidatesModel.model.find().exec();
    }

    public async saveCandidate(candidate: ICandidate): Promise<void> {
        const existingCandidate = await CandidatesModel.model.findOne({MA: candidate.MA});
        if (existingCandidate) {
            if (existingCandidate.isDeleted) {
                existingCandidate.isDeleted = false;
                await existingCandidate.save();
            }
        } else {
            await CandidatesModel.model.create(candidate);
        }
    }

    public async deleteAllCandidates(): Promise<void> {
        await CandidatesModel.model.updateMany({isDeleted: false}, {isDeleted: true});
    }
}

export interface ICandidate {
    MA: number,
    name: string,
    tafkid: string,
    age: number,
    vetek: number,
    gender: string,
    darga: string,
    slavesSum: number,
    merkaz: string,
    anaf: string,
    mador: string,
    isDeleted?: boolean
}

const candidateSchema = new Schema<ICandidate>({
    MA: Number,
    name: String,
    tafkid: String,
    age: Number,
    vetek: Number,
    gender: String,
    darga: String,
    slavesSum: Number,
    merkaz: String,
    anaf: String,
    mador: String,
    isDeleted: {type: Boolean, default: false}
});
