import {CyclesModel, ICycle} from "./cycles-model";

export class CyclesModule {
    private static cyclesModel: CyclesModel;

    public static init() {
        CyclesModule.cyclesModel = new CyclesModel();
    }

    public static async getAllCycles(): Promise<ICycle[]> {
        return await CyclesModule.cyclesModel.getAllCycles();
    }

    public static async getLastCycle(): Promise<any> {
        return await CyclesModule.cyclesModel.getLastCycle();
    }

    public static async createCycle(name: string): Promise<ICycle> {
        return await CyclesModule.cyclesModel.createCycle(name);
    }

    public static async deleteCycle(id: string): Promise<void> {
        await CyclesModule.cyclesModel.deleteCycle(id);
    }
}