import * as mongoose from "mongoose";
import {Model, Schema} from "mongoose";

export class CyclesModel {
    private static model: Model<ICycle>;

    public constructor() {
        CyclesModel.model = mongoose.model<ICycle>('cycles', cyclesSchema);
    }

    public async getAllCycles(): Promise<ICycle[]> {
        return CyclesModel.model.find().exec();
    }

    public async getLastCycle(): Promise<any> {
        return await CyclesModel.model.findOne().sort({_id: -1}).limit(1).exec();
    }

    public async createCycle(name: string): Promise<ICycle> {
        return await CyclesModel.model.create({name: name});
    }

    public async deleteCycle(id: string): Promise<void> {
        await CyclesModel.model.findByIdAndRemove(id);
    }
}

export interface ICycle {
    _id: string,
    name: string,
}

const cyclesSchema = new Schema<ICycle>({
    name: String
});
