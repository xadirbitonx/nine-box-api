import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import dotenv from "dotenv";
import {createIndexRoute} from "./routes/index";
import {CandidatesModule} from "./modules/candidates/candidates-module";
import {QuizesModule} from "./modules/quizes/quizes-module";
import {SotzyometryModule} from "./modules/sotzyometry/sotzyometry-module";
import {CyclesModule} from "./modules/cycles/cycles-module";
dotenv.config();

function getApp() {
    const app = express();

    const corsOptions = {
        origin: 'http://localhost:4200',
        optionsSuccessStatus: 200
    };
    app.use(cors(corsOptions));
    app.use(express.json());
    app.use(express.urlencoded({extended: false}));

    // Routes
    app.use('/', createIndexRoute());

    return app;
}

async function initServer(): Promise<number> {
    // connect to db
    await mongoose.connect('mongodb://localhost/nine-box');
    console.log("connected to db");

    // init modules
    CandidatesModule.init();
    QuizesModule.init();
    SotzyometryModule.init();
    CyclesModule.init();

    return 5000;
}

async function startApp(): Promise<void> {
    const port = await initServer();
    const app = getApp();

    app.listen(port, () => {
        console.log("listening on port " + port);
    });
}

startApp().catch(err => {
    console.log("fuck");
    process.exit(1);
});
